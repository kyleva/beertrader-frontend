import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'

import list from './list'
import post from './post'
import user from './user'
import profile from './profile'

export default combineReducers({
  routing: routerReducer,
  list,
  post,
  user,
  profile
})
