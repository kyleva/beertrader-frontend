export const REQUEST_PROFILE = 'REQUEST_PROFILE'
export const RECEIVED_PROFILE = 'RECEIVED_PROFILE'

const initialState = {
  username: '',
  posts: [],
  comments: []
}

export default (state = initialState, action) => {
  const data = action.data

  switch (action.type) {
    case REQUEST_PROFILE:
      return {
        ...state,
        isLoading: true
      }

    case RECEIVED_PROFILE:
      return {
        comments: data.comments,
        isLoading: false,
        posts: data.posts
      }

    default:
      return state
  }
}

export const requestProfile = username => {
  return dispatch => {
    dispatch({
      type: REQUEST_PROFILE
    })

    getProfileByUsername(username).then(profile => {
      console.log(profile)
      dispatch({
        type: RECEIVED_PROFILE,
        data: {
          ...profile['data']
        }
      })
    })
  }
}

const getProfileByUsername = username => {
  return fetch(`//localhost:9090/user/${username}`, {
    type: 'GET',
    contentType: 'application/json'
  }).then(res => res.json())
}
