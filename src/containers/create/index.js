import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { push } from 'react-router-redux'
import cookie from 'js-cookie'

import { createPost } from './../../modules/post'
import { destroyUser } from './../../modules/user'

class Create extends Component {
  constructor() {
    super()

    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.signUserOut = this.signUserOut.bind(this)
  }

  state = {
    title: '',
    description: ''
  }

  handleChange(event) {
    const name = event.target.getAttribute('name')
    this.setState({ [name]: event.target.value })
  }

  handleSubmit(event) {
    event.preventDefault()

    const { title, description } = this.state
    const { userId } = this.props
    if (!title && !description) return
    const token = cookie.get('token')
    this.props.createPost({ title, description, userId, token })
  }

  signUserOut() {
    destroyUser()
  }

  render() {
    // We need to not show this page if a user is not signed in
    // We can do this with userId or user.loggedIn
    if (this.props.error) {
      this.signUserOut()
    }

    if (this.props.id) {
      this.props.redirectToPost(`${this.props.id}`)
      return null
    } else {
      const { title, description } = this.state
      return (
        <form onSubmit={this.handleSubmit}>
          <div className="error">{this.props.error}</div>
          <div className="Create">
            Title:{' '}
            <input
              name="title"
              type="text"
              value={title}
              onChange={this.handleChange}
            />{' '}
            <br />
            Description:{' '}
            <textarea
              name="description"
              value={description}
              onChange={this.handleChange}
            />
          </div>
          <button>Create post</button>
        </form>
      )
    }
  }
}

const mapStateToProps = state => ({
  id: state.post.id,
  userId: state.user.id,
  error: state.post.error
})

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      createPost,
      redirectToPost: id => push(`/post/${id}`)
    },
    dispatch
  )

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Create)
