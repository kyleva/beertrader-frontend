import React from 'react'

export default props => (
  <div onClick={() => props.onClick(props.id)} data-id={props.id}>
    <h4>{props.title}</h4>
    <p>
      <small>{props.user.username}</small>
    </p>
  </div>
)
