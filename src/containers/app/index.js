import React from 'react'
import { Route, Link } from 'react-router-dom'

import List from '../list'
import Login from '../login'
import Post from '../post'
import Create from '../create'
import Profile from '../profile'
import Register from '../register'

import UserUtility from '../user-utility'

const App = () => (
  <div>
    <header>
      <UserUtility />
      <h1>
        <u>beerswap.io</u>
      </h1>
    </header>

    <main>
      <Route exact path="/" component={List} />
      <Route exact path="/login" component={Login} />
      <Route exact path="/post/:id" component={Post} />
      <Route exact path="/post" component={Create} />
      <Route exact path="/user/:username" component={Profile} />
      <Route exact path="/register" component={Register} />
    </main>
  </div>
)

export default App
