export const POSTS_REQUESTED = 'POSTS_REQUESTED'
export const POSTS_RECEIVED = 'POSTS_RECEIVED'

const initialState = {
  posts: [],
  isLoading: false
}

export default (state = initialState, action) => {
  const data = action.data
  switch (action.type) {
    case POSTS_REQUESTED:
      return {
        ...state,
        isLoading: true
      }

    case POSTS_RECEIVED:
      const posts = data || []
      return {
        ...state,
        isLoading: false,
        posts: posts
      }

    default:
      return state
  }
}

export const requestPosts = () => {
  return dispatch => {
    dispatch({ type: POSTS_REQUESTED })

    fetchPosts().then(posts => {
      dispatch({
        type: POSTS_RECEIVED,
        data: posts.data
      })
    })
  }
}

function fetchPosts() {
  return fetch('//localhost:9090/posts', {
    type: 'GET',
    contentType: 'application/json'
  }).then(res => res.json())
}
