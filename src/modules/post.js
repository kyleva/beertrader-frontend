export const REQUEST_POST = 'REQUEST_POST'
export const RECEIVED_POST = 'RECEIVED_POST'
export const CREATING_POST = 'CREATING_POST'
export const CREATED_POST = 'CREATED_POST'
export const ERROR_CREATING_POST = 'ERROR_CREATING_POST'

export const ADDING_COMMENT = 'ADDING_COMMENT'
export const ADDED_COMMENT = 'ADDED_COMMENT'

const initialState = {
  comments: [],
  description: '',
  id: '',
  isLoading: false,
  createdSuccess: false,
  title: '',
  username: '',
  error: ''
}

export default (state = initialState, action) => {
  const data = action.data
  switch (action.type) {
    case CREATING_POST:
      return {
        ...state,
        isLoading: true
      }

    case CREATED_POST:
      return {
        ...state,
        createdSuccess: true,
        id: data.postId
      }

    case ERROR_CREATING_POST:
      return {
        ...state,
        isLoading: false,
        error: data.error
      }

    case REQUEST_POST:
      return {
        ...state,
        isLoading: true
      }

    case ADDING_COMMENT:
      return state

    case ADDED_COMMENT:
      return {
        ...state,
        comments: [
          {
            id: data.id,
            body: data.body,
            username: data.username
          },
          ...state.comments
        ]
      }

    case RECEIVED_POST:
      return {
        ...state,
        comments: data.comments || [],
        description: data.body,
        id: data.id,
        isLoading: false,
        title: data.title,
        username: data.user.username
      }

    default:
      return state
  }
}

export const requestPost = id => {
  return dispatch => {
    dispatch({
      type: REQUEST_POST
    })

    fetchPost(id).then(post => {
      dispatch({
        type: RECEIVED_POST,
        data: {
          ...post['data']
        }
      })
    })
  }
}

export const createPost = post => {
  return dispatch => {
    dispatch({
      type: CREATING_POST
    })

    submitPost(post).then(res => {
      if (res.error) {
        dispatch({
          type: ERROR_CREATING_POST,
          data: {
            error: res.error
          }
        })
        return
      }
      dispatch({
        type: CREATED_POST,
        data: {
          postId: post.id
        }
      })
    })
  }
}

export const addCommentToPost = (comment, username, userId, postId) => {
  return dispatch => {
    dispatch({
      type: ADDING_COMMENT
    })

    addComment(comment, username, userId, postId).then(comment => {
      dispatch({
        type: ADDED_COMMENT,
        data: {
          id: comment.id,
          body: comment.body,
          username: comment.username
        }
      })
    })
  }
}

const addComment = (comment, username, userId, postId) => {
  return fetch('//localhost:9090/comment', {
    body: JSON.stringify({
      body: comment,
      username: username,
      userId: userId,
      postId: postId
    }),
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer `
    }
  }).then(res => res.json())
}

const fetchPost = id => {
  return fetch(`//localhost:9090/post/${id}`, {
    body: JSON.stringify({
      id: id
    }),
    method: 'POST',
    headers: { 'Content-Type': 'application/json' }
  }).then(res => res.json())
}

const submitPost = post => {
  const { title, description, userId, token } = post
  return fetch('//localhost:9090/post', {
    body: JSON.stringify({
      title,
      description,
      userId,
      token
    }),
    method: 'POST',
    headers: { 'Content-Type': 'application/json' }
  }).then(res => res.json())
}
