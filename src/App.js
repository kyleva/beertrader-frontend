import React, { Component } from 'react'
import logo from './logo.svg'
import './App.css'

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <p style="float:right">
            <small>
              <a href="http://localhost:9090/auth/reddit">Sign in.</a>
            </small>
          </p>
          <h1 className="App-title">app: beerswap.io</h1>
        </header>
        <p className="App-intro">
          Welcome to Beertrader. <br />
          <a href="http://localhost:9090/auth/reddit">Login to reddit.</a>
        </p>
      </div>
    )
  }
}

export default App
