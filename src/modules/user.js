import Cookies from 'js-cookie'
import decode from 'jwt-decode'
import { push } from 'react-router-redux'

import { base64decode, getCookieValue } from './../utility'

export const USER_CREATE_ERROR = 'USER_CREATE_ERROR'
export const USER_CREATE = 'USER_CREATE'
export const USER_CREATED = 'USER_CREATED'
export const USER_SIGN_IN = 'USER_SIGN_IN'
export const USER_SIGNED_IN = 'USER_SIGNED_IN'
export const USER_DESTROY = 'USER_DESTROY'

// TODO: Align action names: user sign in or user log in
// Leaning towards "sign-in"

// TODO: Convert `data` key to `payload` standard

const initialState = {
  id: 0,
  loggedIn: false,
  username: '',
  verified: false,
}

export default (state = initialState, action) => {
  switch (action.type) {
    case USER_CREATE_ERROR:
      return {
        errorMessage: action.data.errorMessage,
      }

    case USER_CREATED:
      return {
        ...state,
        id: action.data.id,
        username: action.data.username,
        verified: false,
      }

    case USER_SIGNED_IN:
      return {
        ...state,
        loggedIn: true,
        username: action.data.username,
        id: action.data.id,
      }

    case USER_DESTROY:
      return {
        ...state,
        loggedIn: false,
        username: '',
        id: 0,
      }

    default:
      return state
  }
}

export const bootstrapUser = () => {
  console.log('boots')
  return dispatch => {
    const token = getCookieValue('token')

    console.log(token)

    if (!token) return

    const decoded = decode(token)

    console.log(decoded)

    if (decoded.username) {
      dispatch({
        type: USER_SIGNED_IN,
        data: {
          username: decoded.username,
          id: decoded.id,
        },
      })
    }
  }
}

export const destroyUser = () => {
  return dispatch => {
    dispatch({
      type: USER_DESTROY,
    })
  }
}

// TODO: Create fetch abstraction

export const createUser = props => {
  return dispatch => {
    dispatch({
      type: USER_CREATE,
    })

    registerUser(props).then(res => {
      console.log(res)
      if (res.isError) {
        dispatch({
          type: USER_CREATE_ERROR,
          data: {
            errorMessage: res.error,
          },
        })
      } else {
        let user = res.user

        dispatch({
          type: USER_CREATED,
          data: {
            id: user.id,
            username: user.username,
            verified: false,
          },
        })

        window.location.href = 'http://localhost:3000'
      }
    })
  }
}

export const loginUser = props => {
  return dispatch => {
    dispatch({
      type: USER_SIGN_IN,
    })

    // figure out standardized structure for:
    // - errors coming back alongside data
    // - prob do error first then response success stuff

    // TODO: res is coming through truthy when inputs are empty
    // investigate why and fix
    requestLoginUser(props).then(res => {
      console.log(res)
      if (res.isError) {
        return console.log(res.message)
      }
      Cookies.set('token', res.token, { expires: 1 })
      window.location.href = 'http://localhost:3000'
    })
  }
}

const requestLoginUser = user => {
  const { password, username } = user

  return fetch('//localhost:9090/user/login', {
    body: JSON.stringify({
      password,
      username,
    }),
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
  }).then(res => res.json())
}

const registerUser = user => {
  const { email, password, username } = user

  return fetch('//localhost:9090/user/create', {
    body: JSON.stringify({
      email,
      password,
      username,
    }),
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
  }).then(res => res.json())
}
