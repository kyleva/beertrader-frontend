import React, { Component } from 'react'
import { push } from 'react-router-redux'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import { requestPosts } from '../../modules/list'
import ListItem from '../list-item'

class List extends Component {
  componentDidMount() {
    this.props.requestPosts()
  }

  render() {
    return (
      <div>
        <h3>recent posts</h3>
        <div>
          {this.props.posts.map(post => {
            return (
              <ListItem {...post} key={post.id} onClick={this.props.goToPost} />
            )
          })}
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  posts: state.list.posts,
  isLoading: state.list.isLoading
})

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      requestPosts,
      goToPost: id => push(`/post/${id}`)
    },
    dispatch
  )

export default connect(mapStateToProps, mapDispatchToProps)(List)
