import React, { Component } from 'react'

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import { loginUser } from './../../modules/user'

class Login extends Component {
  constructor(props) {
    super(props)

    this.state = {
      password: '',
      username: '',
    }

    this.dispatchLoginUser = this.dispatchLoginUser.bind(this)
  }

  dispatchLoginUser() {
    this.props.loginUser({
      email: this.state.email,
      password: this.state.password,
      username: this.state.username,
    })
  }

  updateInputValue(event) {
    const name = event.target.getAttribute('name')

    this.setState({
      [name]: event.target.value,
    })
  }

  render() {
    return (
      <div className="account-form">
        <label>
          Username
          <input
            type="text"
            className="field-username"
            name="username"
            value={this.state.username}
            onChange={event => this.updateInputValue(event)}
          />
        </label>

        <br />
        <br />

        <label>
          Password
          <input
            type="password"
            className="field-password"
            name="password"
            value={this.state.password}
            onChange={event => this.updateInputValue(event)}
          />
        </label>

        <br />
        <br />

        <input type="submit" onClick={this.dispatchLoginUser} />
      </div>
    )
  }
}

const mapStateToProps = state => ({
  username: state.user.username,
  loggedIn: state.user.loggedIn,
})

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      loginUser,
    },
    dispatch,
  )

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Login)
