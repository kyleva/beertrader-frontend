import React from 'react'

export default props => (
  <div className="Post__comments">
    {props.comments.map(comment => {
      return (
        <div className="Comment" key={comment.id}>
          <p>
            <small>{comment.username}</small>
            {comment.body}
          </p>
        </div>
      )
    })}
  </div>
)
