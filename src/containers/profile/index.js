import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { push } from 'react-router-redux'

import { requestProfile } from './../../modules/profile'

class Profile extends Component {
  componentDidMount() {
    const username = this.props.match.params.username
    this.props.requestProfile(username)
  }

  render() {
    const username = this.props.username
    const gotoPost = this.props.gotoPost

    return (
      <div className="Profile">
        <h2>user: {username}</h2>

        <h3>Posts</h3>
        <ul>
          {this.props.posts.map(post => {
            return <li onClick={() => gotoPost(post.id)}>{post.title}</li>
          })}
        </ul>

        <h3>Comments</h3>
        <ul>
          {this.props.comments.map(comment => {
            return <li>{comment.body}</li>
          })}
        </ul>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  comments: state.profile.comments,
  posts: state.profile.posts,
  username: state.user.username
})

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      requestProfile,
      gotoPost: id => push(`/post/${id}`)
    },
    dispatch
  )

export default connect(mapStateToProps, mapDispatchToProps)(Profile)
