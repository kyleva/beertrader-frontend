export const base64decode = string => {
  const characters =
    'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/='
  let result = ''

  let i = 0
  do {
    var b1 = characters.indexOf(string.charAt(i++))
    var b2 = characters.indexOf(string.charAt(i++))
    var b3 = characters.indexOf(string.charAt(i++))
    var b4 = characters.indexOf(string.charAt(i++))

    var a = ((b1 & 0x3f) << 2) | ((b2 >> 4) & 0x3)
    var b = ((b2 & 0xf) << 4) | ((b3 >> 2) & 0xf)
    var c = ((b3 & 0x3) << 6) | (b4 & 0x3f)

    result +=
      String.fromCharCode(a) +
      (b ? String.fromCharCode(b) : '') +
      (c ? String.fromCharCode(c) : '')
  } while (i < string.length)

  return result
}

export const getCookieValue = a => {
  var b = document.cookie.match('(^|;)\\s*' + a + '\\s*=\\s*([^;]+)')
  return b ? b.pop() : ''
}
