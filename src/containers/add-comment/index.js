import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import { addCommentToPost } from './../../modules/post'

class AddComment extends Component {
  constructor() {
    super()

    this.handleChange = this.handleChange.bind(this)
    this.submitComment = this.submitComment.bind(this)
  }

  state = {
    comment: '',
  }

  handleChange(event) {
    this.setState({ comment: event.target.value })
  }

  submitComment() {
    const { comment } = this.state
    const { postId, userId, username } = this.props
    if (!comment) return
    this.props.addCommentToPost(comment, username, userId, postId)
    this.setState({ comment: '' })
  }

  render() {
    return (
      <div className="AddComment">
        <div className="AddComment__textarea-wrap">
          <textarea
            className="AddComment__textarea"
            value={this.state.comment}
            onChange={this.handleChange}
          />
          <button className="AddComment__submit" onClick={this.submitComment}>
            Add comment
          </button>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  userId: state.user.id,
  username: state.user.username,
  postId: state.post.id,
})

const mapDispatchToProps = dispatch =>
  bindActionCreators({ addCommentToPost }, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AddComment)
