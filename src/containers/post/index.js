import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import AddComment from './../add-comment'
import Comments from './../comments'
import { requestPost } from './../../modules/post'

class Post extends React.Component {
  componentDidMount() {
    const id = this.props.match.params.id
    this.props.requestPost(id)
  }

  render() {
    const { comments, title, description, username, id } = this.props
    return (
      <div className="Post">
        <div className="Post__details">
          <p>
            Title: {title} <br />
            Description: {description} <br />
            User: {username} <br />
          </p>
        </div>

        <AddComment />
        <Comments comments={this.props.comments} />
      </div>
    )
  }
}

const mapStateToProps = state => ({
  comments: state.post.comments,
  description: state.post.description,
  id: state.post.id,
  title: state.post.title,
  username: state.post.username
})

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      requestPost
    },
    dispatch
  )

export default connect(mapStateToProps, mapDispatchToProps)(Post)
