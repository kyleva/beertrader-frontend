import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { push } from 'react-router-redux'

import { bootstrapUser } from './../../modules/user'

class UserUtility extends Component {
  componentDidMount() {
    this.props.bootstrapUser()
  }

  render() {
    const userIsLoggedIn = this.props.loggedIn
    let utility
    if (userIsLoggedIn) {
      utility = `Hello ${this.props.username}.`
    } else {
      utility = <button onClick={this.props.goToLogin}>Login/Register</button>
    }
    return (
      <div>
        <p>
          <small>{utility}</small>
        </p>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  username: state.user.username,
  loggedIn: state.user.loggedIn
})

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      bootstrapUser,
      goToLogin: () => push('/login')
    },
    dispatch
  )

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserUtility)
