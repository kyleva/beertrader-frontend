import React, { Component } from 'react'

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import { createUser } from './../../modules/user'

class Register extends Component {
  constructor(props) {
    super(props)

    this.state = {
      email: '',
      password: '',
      username: '',
    }

    this.dispatchCreateUser = this.dispatchCreateUser.bind(this)
  }

  dispatchCreateUser() {
    this.props.createUser({
      email: this.state.email,
      password: this.state.password,
      username: this.state.username,
    })
  }

  updateInputValue(event) {
    const name = event.target.getAttribute('name')

    this.setState({
      [name]: event.target.value,
    })
  }

  render() {
    return (
      <div className="account-form">
        <label>
          Username
          <input
            type="text"
            className="field-username"
            name="username"
            value={this.state.username}
            onChange={event => this.updateInputValue(event)}
          />
        </label>

        <br />
        <br />

        <label>
          Email
          <input
            type="text"
            className="field-email"
            name="email"
            value={this.state.email}
            onChange={event => this.updateInputValue(event)}
          />
        </label>

        <br />
        <br />

        <label>
          Password
          <input
            type="password"
            className="field-password"
            name="password"
            value={this.state.password}
            onChange={event => this.updateInputValue(event)}
          />
        </label>

        <br />
        <br />

        <input type="submit" onClick={this.dispatchCreateUser} />
      </div>
    )
  }
}

const mapStateToProps = state => ({
  username: state.user.username,
  loggedIn: state.user.loggedIn,
})

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      createUser,
    },
    dispatch,
  )

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Register)
